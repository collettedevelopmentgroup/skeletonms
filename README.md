# SkeletonMS

[![PHP](https://img.shields.io/badge/PHP-%5E8.0-blueviolet)](https://www.php.net/manual/en/)
[![poweredby](https://img.shields.io/badge/powered%20by-Slim4-green)](https://www.slimframework.com/docs/v4/)
![Bitbucket open issues](https://img.shields.io/bitbucket/issues/collettedevelopmentgroup/skeletonms)

Skeleton MicroService powered by [Slim4 Framework](https://www.slimframework.com/).

## Installation

Use Composer to install SkeletonMS locally.

```bash
composer create-project fr8train/skeleton-ms [directory] [version] 
```

## Post-Installation

Please create a .env file at the root directory of the project. A blank one is required for the project to run, and we recommend that you use it as the location to store any sensitive information such as passwords or keys. To retrieve any data stored here, use [vlucas/phpdotenv](https://packagist.org/packages/vlucas/phpdotenv).

If for some reason you get an error message indicating that a class in the code cannot be found, first try to reload the autoloaded classes through Composer.

```bash
composer dump-autoload
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)