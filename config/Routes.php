<?php

namespace config;

use controllers\BaseController;
use controllers\HelloWorldController;
use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Routes
{
    public static function register(App $app) {
        $app->get('/', function (Request $request, Response $response) {
            $controller = new BaseController();
            return $controller->json($response, [ 'message' => 'You have reached us. Please leave us a message.' ]);
        });

        $app->get('/hello', HelloWorldController::class.':get');
    }
}