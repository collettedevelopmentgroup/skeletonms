<?php


namespace controllers;

use Monolog\Logger;
use Psr\Http\Message\ResponseInterface as Response;
use Exception;

class BaseController
{
    public function json(Response $response, $payload, $httpCode = 200) : Response
    {
        $status = [
            'status' => $httpCode === 200 ? 'OK' : 'error'
        ];

        $response->getBody()->write(json_encode(array_merge($status, $payload)));

        return $response->withHeader('Content-Type', 'application/json')
            ->withStatus($httpCode);
    }

    public function error(Response $response, Logger $log, Exception $exception) : Response {
        $log->error(json_encode([
            'message' => $exception->getMessage(),
            'trace' => $exception->getTrace()
        ]));

        return $this->json($response, [
            'error' => [
                'message' => $exception->getMessage()
            ]
        ], 500);
    }
}