<?php


namespace controllers;

use factories\LoggerFactory;
use Monolog\Logger;
use services\HelloWorldService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class HelloWorldController extends BaseController
{
    protected Logger $log;

    public function __construct()
    {
        $this->log = LoggerFactory::createLogger("HelloWorldController");
    }

    public function get(Request $request, Response $response, array $args): Response
    {
        $payload = $request->getParsedBody();
        $response->getBody()->write(HelloWorldService::hello((array)$payload, $args));
        return $response;
    }
}