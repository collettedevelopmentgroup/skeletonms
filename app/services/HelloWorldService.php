<?php


namespace services;


class HelloWorldService
{
    /**
     * @param array $payload
     * @param array $args
     * @return array
     */
    public static function hello(array $payload, array $args) : array {
        return [
            "text" => "Hello World! Happy Halloween!",
            "payload" => $payload,
            "args" => $args
        ];
    }
}