<?php

namespace factories;

class ObjectFactory
{
    public static array $subModels = [];

    public static function loadClass(string $className, object $data): mixed
    {
        $object = new $className();
        $properties = get_object_vars($object);

        foreach ($properties as $property => $value) {
            $propertyType = gettype($object->{$property});

            if ($propertyType == 'object') {
                $propertyClass = get_class($object->{$property});

                if (in_array($propertyClass, self::$subModels) && isset($data->{$property})) {
                    $object->{$property} = self::loadClass($propertyClass, $data->{$property});
                }
            } elseif (property_exists($data, $property)) {
                $object->{$property} = $data->{$property};
            }
        }

        return $object;
    }
}